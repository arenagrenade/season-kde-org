<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $myKdeId;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="student", orphanRemoval=true)
     */
    private $projects;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Project", mappedBy="mentors")
     */
    private $projectsMentored;

    /**
     * @ORM\Column(type="boolean")
     */
    private $mentor;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\MentorApplication", mappedBy="user", cascade={"persist", "remove"})
     */
    private $mentorApplication;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
        $this->projectsMentored = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->username;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return '';
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return mixed
     */
    public function getMyKdeId()
    {
        return $this->myKdeId;
    }

    /**
     * @param mixed $myKdeId
     */
    public function setMyKdeId($myKdeId): void
    {
        $this->myKdeId = $myKdeId;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setStudent($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            // set the owning side to null (unless already changed)
            if ($project->getStudent() === $this) {
                $project->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjectsMentored(): Collection
    {
        return $this->projectsMentored;
    }

    public function addProjectsMentored(Project $projectsMentored): self
    {
        if (!$this->projectsMentored->contains($projectsMentored)) {
            $this->projectsMentored[] = $projectsMentored;
            $projectsMentored->addMentor($this);
        }

        return $this;
    }

    public function removeProjectsMentored(Project $projectsMentored): self
    {
        if ($this->projectsMentored->contains($projectsMentored)) {
            $this->projectsMentored->removeElement($projectsMentored);
            $projectsMentored->removeMentor($this);
        }

        return $this;
    }

    /**
     * @return boolean
     */
    public function isMentor()
    {
        return $this->mentor;
    }

    /**
     * @param boolean $mentor
     */
    public function setMentor($mentor): void
    {
        $this->mentor = $mentor;
    }

    public function getMentorApplication(): ?MentorApplication
    {
        return $this->mentorApplication;
    }

    public function setMentorApplication(MentorApplication $mentorApplication): self
    {
        $this->mentorApplication = $mentorApplication;

        // set the owning side of the relation if necessary
        if ($mentorApplication->getUser() !== $this) {
            $mentorApplication->setUser($this);
        }

        return $this;
    }

    public function setAddress(string $address): void
    {
	$this->address = $address;
    }

    public function getAddress(): ?string
    {
	return $this->address;
    }
}
