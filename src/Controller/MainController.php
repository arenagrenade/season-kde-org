<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Season;
use App\Entity\User;
use App\Form\ProfileType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @Route("/profile", name="profile")
     */
    public function profile(EntityManagerInterface $em, Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();
            $flashBag->add('info', 'You edited your address.');
            return $this->render('profile.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        return $this->render('profile.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/", name="homepage")
     */
    public function index(EntityManagerInterface $em)
    {
        return $this->render('homepage.html.twig');
    }

    /**
     * @Route("/connect/mykde", name="connect_mykde")
     */
    public function connectMyKde(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('mykde') // key used in config/packages/knpu_oauth2_client.yaml
            ->redirect([
                'email',
            ], [])
        ;

	}

    /**
     * @Route("/connect/mykde/check", name="connect_mykde_check")
     */
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry)
    {
    }

    /**
     * @Route("/logout", name="app_logout", methods={"GET"})
     */
    public function logout()
    {
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }
}
