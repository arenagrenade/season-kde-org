<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SeasonRepository")
 */
class Season
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startTime;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="datetime")
     */
    private $deadlineStudent;

    /**
     * @ORM\Column(type="datetime")
     */
    private $deadlineMentor;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="season", orphanRemoval=true)
     */
    private $projects;

    /**
     * @ORM\Column(type="datetime")
     */
    private $projectsAnnouncement;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startWork;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endWork;

    /**
     * @ORM\Column(type="datetime")
     */
    private $resultAnnouncement;

    /**
     * @ORM\Column(type="datetime")
     */
    private $certificatesIssued;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    public function setStartTime(\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }


    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getDeadlineStudent(): ?\DateTimeInterface
    {
        return $this->deadlineStudent;
    }

    public function setDeadlineStudent(\DateTimeInterface $deadlineStudent): self
    {
        $this->deadlineStudent = $deadlineStudent;

        return $this;
    }

    public function getDeadlineMentor(): ?\DateTimeInterface
    {
        return $this->deadlineMentor;
    }

    public function setDeadlineMentor(\DateTimeInterface $deadlineMentor): self
    {
        $this->deadlineMentor = $deadlineMentor;

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setSeason($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            // set the owning side to null (unless already changed)
            if ($project->getSeason() === $this) {
                $project->setSeason(null);
            }
        }

        return $this;
    }

    public function getProjectsAnnouncement(): ?\DateTimeInterface
    {
        return $this->projectsAnnouncement;
    }

    public function setProjectsAnnouncement(\DateTimeInterface $projectsAnnouncement): self
    {
        $this->projectsAnnouncement = $projectsAnnouncement;

        return $this;
    }

    public function getStartWork(): ?\DateTimeInterface
    {
        return $this->startWork;
    }

    public function setStartWork(\DateTimeInterface $startWork): self
    {
        $this->startWork = $startWork;

        return $this;
    }

    public function getEndWork(): ?\DateTimeInterface
    {
        return $this->endWork;
    }

    public function setEndWork(\DateTimeInterface $endWork): self
    {
        $this->endWork = $endWork;

        return $this;
    }

    public function getResultAnnouncement(): ?\DateTimeInterface
    {
        return $this->resultAnnouncement;
    }

    public function setResultAnnouncement(\DateTimeInterface $resultAnnouncement): self
    {
        $this->resultAnnouncement = $resultAnnouncement;

        return $this;
    }

    public function getCertificatesIssued(): ?\DateTimeInterface
    {
        return $this->certificatesIssued;
    }

    public function setCertificatesIssued(\DateTimeInterface $certificatesIssued): self
    {
        $this->certificatesIssued = $certificatesIssued;

        return $this;
    }
}
