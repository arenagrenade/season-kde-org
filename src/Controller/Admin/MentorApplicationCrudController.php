<?php

namespace App\Controller\Admin;

use App\Entity\MentorApplication;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MentorApplicationCrudController extends AbstractCrudController
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public static function getEntityFqcn(): string
    {
        return MentorApplication::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Mentor Application')
            ->setEntityLabelInPlural('Mentor Applications')
            ->setSearchFields(['id', 'reason']);
    }

    public function configureFields(string $pageName): iterable
    {
        $date = DateTimeField::new('date');
        $reason = TextareaField::new('reason');
        $user = AssociationField::new('user');
        $id = IntegerField::new('id', 'ID');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $date, $user];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $date, $reason, $user];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$date, $reason, $user];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$date, $reason, $user];
        }
    }

    public function configureActions(Actions $actions): Actions
    {
        $acceptApplication = Action::new('acceptApplication','Accept')->linkToCrudAction('acceptAction');

        $refuseApplication = Action::new('refuseApplication','Refuse')->linkToCrudAction('refuseAction');
        return $actions
            ->add(Crud::PAGE_INDEX, $refuseApplication)
            ->add(Crud::PAGE_INDEX, $acceptApplication);


    }

    public function acceptAction(AdminContext $context)
    {
        $id=$context->getEntity()->getInstance()->getId();;
        $em = $this->getDoctrine()->getManager();
        $mentorApplication = $em->getRepository(MentorApplication::class)->find($id);
        $user = $mentorApplication->getUser();
        $user->setMentor(true);
        //$url = $_ENV['OAUTH_MYKDE_URL'].'/api/badger/grant/sok-mentor/'.$user->getMyKdeId();

        //Using default token for mykde test database if env variable not set
        //try {
        //    $this->client->request('POST', $url, [
        //        'headers' => [
        //            'Accept' => 'application/json',
        //            'Authorization'=> 'Bearer '.(isset($_ENV['BADGER_ACCESS_TOKEN'])?$_ENV['BADGER_ACCESS_TOKEN']:'vlHHRnCxf33G23S2B8G5GO2sV5fJqO') ,
        //        ],
        //    ]);
        //} catch (TransportExceptionInterface $e) {
        //    echo $e->getMessage();
        //}
        $em->remove($mentorApplication);
        $em->flush();
        $crudUrlGenerator = $this->get(CrudUrlGenerator::class);

        return $this->redirect($crudUrlGenerator->build()->unsetAll()->setController(MentorApplicationCrudController::class));
    }

    public function refuseAction(AdminContext $context)
    {
        $id=$context->getEntity()->getInstance()->getId();
        $em = $this->getDoctrine()->getManager();
        $mentorApplication = $em->getRepository(MentorApplication::class)->find($id);
        $em->remove($mentorApplication);
        $em->flush();
        $crudUrlGenerator = $this->get(CrudUrlGenerator::class);

        return $this->redirect($crudUrlGenerator->build()->unsetAll()->setController(MentorApplicationCrudController::class));
    }

}
