<?php


namespace App\EventSubscriber;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Twig\Environment;
use Symfony\Component\HttpKernel\KernelEvents;

class TwigGlobalSubscriber implements EventSubscriberInterface
{
    /**
     * @var Environment
     */
    private $twig;
    /**
     * @var EntityManager
     */
    private $manager;

    public function __construct( Environment $twig, EntityManagerInterface $manager ) {
        $this->twig    = $twig;
        $this->manager = $manager;
    }

    public function injectGlobalVariables() {
        $season = $this->manager->getRepository( 'App:Season' )->findOneBy( [ 'active' => true ] );
        $this->twig->addGlobal( 'season', $season );
    }

    public static function getSubscribedEvents() {
        return [ KernelEvents::CONTROLLER =>  'injectGlobalVariables' ];
    }
}