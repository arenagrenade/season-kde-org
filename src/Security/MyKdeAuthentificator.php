<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Security;

use App\Entity\User;
use App\Providers\MyKdeUser;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class MyKdeAuthentificator extends SocialAuthenticator
{
    private $em;
    private $router;
    private $flashBag;
    private $clientRegistry;

    public function __construct(ClientRegistry $clientRegistry, EntityManagerInterface $em, RouterInterface $router, FlashBagInterface $flashBag)
    {
        $this->em = $em;
        $this->router = $router;
        $this->flashBag = $flashBag;
        $this->clientRegistry = $clientRegistry;
    }

    /**
     * @inheritDoc
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $this->flashBag->add('error', 'You are not logged in');
        return new RedirectResponse(
            '/',
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request)
    {
        return $request->attributes->get('_route') === 'connect_mykde_check';
    }

    /**
     * @inheritDoc
     */
    public function getCredentials(Request $request)
    {
        return $this->fetchAccessToken($this->getMyKDEClient());
    }

    /**
     * @inheritDoc
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /** @var MyKdeUser $myKdeUser */
        $myKdeUser = $this->getMyKDEClient()
            ->fetchUserFromToken($credentials);

        $email = $myKdeUser->getEMail();

        /** @var User|null $existingUser */
        $existingUser = $this->em->getRepository(User::class)
            ->findOneBy(['myKdeId' => $myKdeUser->getId()]);

        if ($existingUser) {
            $this->updateUser($existingUser, $myKdeUser);
            return $existingUser;
        }

        $user = new User();
        $user->setMyKdeId($myKdeUser->getId());
        $user->setMentor(false);
        $this->updateUser($user, $myKdeUser);
        return $user;
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $message = strtr($exception->getMessageKey(), $exception->getMessageData());

        return new Response($message, Response::HTTP_FORBIDDEN);
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        return new RedirectResponse($this->router->generate('homepage'));
    }

    /**
     * @return OAuth2ClientInterface
     */
    private function getMyKDEClient()
    {
        return $this->clientRegistry->getClient('mykde');
    }


    private function updateUser(User $user, MyKdeUser $myKdeUser)
    {
        $user->setUsername($myKdeUser->getName());
        //if (in_array('season-admins', $myKdeUser->getRole())) {
        //    $user->setRoles(['ROLE_ADMIN']);
        //}

        $this->em->persist($user);
        $this->em->flush();
    }
}
