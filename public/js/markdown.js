var easyMDE = new EasyMDE({
    element: document.getElementById('project_student_description'),
    forceSync: true,
    autosave: {
        enabled: true,
        uniqueId: "season-proposal",
        delay: 1000,
        submit_delay: 5000,
        timeFormat: {
            locale: 'en-US',
            format: {
                year: 'numeric',
                month: 'long',
                day: '2-digit',
                hour: '2-digit',
                minute: '2-digit',
            },
        },
        text: "Autosaved: "
    }});

document.querySelectorAll('.comment').forEach(item => {
    const form = item.dataset.form;
    const parentId = item.dataset.id;
    const formElement = document.createElement("reply");
    formElement.classList.add("card-body");
    formElement.innerHTML = form;
    formElement.querySelector('input[type=hidden]').value = parentId;
    item.appendChild(formElement);

});

