<?php

namespace App\Controller\Admin;

use App\Entity\Project;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ProjectCrudController extends AbstractCrudController
{

    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public static function getEntityFqcn(): string
    {
        return Project::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Project')
            ->setEntityLabelInPlural('Projects')
            ->setSearchFields(['id', 'title', 'description']);
    }

    public function configureFields(string $pageName): iterable
    {
        $title = TextField::new('title');
        $description = TextareaField::new('description');
        $accepted = BooleanField::new('accepted')->setCustomOption('renderAsSwitch', false);
        $completed = BooleanField::new('completed');
        $isVisible = BooleanField::new('isVisible');
        $student = AssociationField::new('student');
        $mentors = AssociationField::new('mentors');
        $season = AssociationField::new('season');
        $id = IntegerField::new('id', 'ID');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $title, $accepted, $completed, $isVisible, $student, $mentors];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $title, $description, $accepted, $completed, $isVisible, $student, $mentors, $season];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$title, $description, $accepted, $completed, $isVisible, $student, $mentors, $season];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$title, $description, $accepted, $completed, $isVisible, $student, $mentors, $season];
        }
    }

    public function configureActions(Actions $actions): Actions
    {
        $acceptProject = Action::new('acceptProject','Accept')->linkToCrudAction('acceptAction')->displayIf(static function ($entity) {
            return !$entity->getAccepted();
        });;

        $rejectProject = Action::new('rejectProject','Reject')->linkToCrudAction('rejectProject')->linkToCrudAction('rejectAction');
        return $actions
            ->add(Crud::PAGE_INDEX, $rejectProject)
            ->add(Crud::PAGE_INDEX, $acceptProject);
    }

    public function acceptAction(AdminContext $context)
    {
        $projectId=$context->getEntity()->getInstance()->getId();;
        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository(Project::class)->find($projectId);
        $user = $project->getStudent();
        $url = $_ENV['OAUTH_MYKDE_URL'].'/api/badger/grant/sok-student/'.$user->getMyKdeId();

        //Using default token for mykde test database if env variable not set
        try {
            $this->client->request('POST', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization'=> 'Bearer '.(isset($_ENV['BADGER_ACCESS_TOKEN'])?$_ENV['BADGER_ACCESS_TOKEN']:'vlHHRnCxf33G23S2B8G5GO2sV5fJqO') ,
                ],
            ]);
        } catch (TransportExceptionInterface $e) {
            echo $e->getMessage();
        }
        $project->setAccepted(true);
        $em->flush();
        $crudUrlGenerator = $this->get(CrudUrlGenerator::class);

        return $this->redirect($crudUrlGenerator->build()->unsetAll()->setController(ProjectCrudController::class));
    }

    public function rejectAction(AdminContext $context)
    {
        $projectId=$context->getEntity()->getInstance()->getId();;
        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository(Project::class)->find($projectId);
        $user = $project->getStudent();
        $project->setAccepted(false);
        $em->flush();
        $crudUrlGenerator = $this->get(CrudUrlGenerator::class);

        return $this->redirect($crudUrlGenerator->build()->unsetAll()->setController(ProjectCrudController::class));
    }

}
