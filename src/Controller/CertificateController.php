<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Season;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class CertificateController extends AbstractController
{
    /**
     * @Route("/{id}/project/{projectId}/certificate", name="certificate")
     * @Entity("Project", expr="repository.find(projectId)")
     * @param Season $season
     * @param Project $project
     * @param Pdf $snappy
     * @return PdfResponse
     */
    public function index(Season $season, Project $project, Pdf $snappy)
    {
        if (new \DateTime() < $season->getCertificatesIssued()) {
            throw new HttpException(403,"Certificates have not been issued yet");
        }

        if ($project->getCompleted() != true) {
            throw new HttpException(403,"You have not completed your project");
        }

        $html = $this->renderView('certificate.html.twig', array(
            'season'  => $season,
            'project' => $project
        ));
        return new PdfResponse(
            $snappy->getOutputFromHtml($html, array(
                'orientation' => 'landscape',
                'no-background' => false,
                'lowquality' => false,
                'page-height' => 238.125,
                'page-width'  => 158.75,
                'encoding' => 'utf-8',
                'images' => true,
                'dpi' => 300,
                'enable-external-links' => true,
                'title'=>'certificate.pdf'
            )),'certficate.pdf'
        );
    }
}
